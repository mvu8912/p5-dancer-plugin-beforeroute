requires 'Dancer';
requires 'Dancer2';
requires "Plack::Test";
requires 'Try::Tiny';
requires 'Test::More';

on testing => sub {
    requires "HTTP::Request::Common";
};
